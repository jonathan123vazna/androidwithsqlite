package com.example.jonathanvazquez.examplesqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.jonathanvazquez.examplesqlite.Database.DatabaseHelper;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    public DatabaseHelper databaseHelper;

    public Button buttonAdd;
    public EditText txtData;
    public ListView userlist;

    public ArrayList<String> listItem;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseHelper = new DatabaseHelper(this);

        listItem = new ArrayList<>();

        txtData = findViewById(R.id.txtData);
        buttonAdd = findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(this);

        userlist = findViewById(R.id.list);

        viewData();

        userlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = userlist.getItemAtPosition(position).toString();
                Toast.makeText(MainActivity.this, "this " + text, Toast.LENGTH_SHORT).show();
                txtData.setText("");
                listItem.clear();
                viewData();
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonAdd:
                String name = txtData.getText().toString();
                if (!name.equals("") && databaseHelper.insertData(name)){
                    Toast.makeText(this, "Data added", Toast.LENGTH_SHORT).show();
                    txtData.setText("");
                }else{
                    Toast.makeText(this, "Data not added", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private void viewData(){
        Cursor cursor = databaseHelper.viewData();
        if (cursor.getCount() == 0){
            Toast.makeText(this, "Not data show", Toast.LENGTH_SHORT).show();
        }else{
            while (cursor.moveToNext()){
                listItem.add(cursor.getString(1)); // index is name, index 0 is Id
            }

            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listItem);
            userlist.setAdapter(adapter);
        }
    }

}
